import { Module } from '@nestjs/common';
import { CustomersService } from './customers.service';
import { CustomersController } from './customers.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { customers } from './entities/customer.entity';

@Module({
  imports: [TypeOrmModule.forFeature([customers])],
  controllers: [CustomersController],
  providers: [CustomersService],
})
export class CustomersModule {}
