import { IsNotEmpty, Length, Min } from 'class-validator';

export class CreateCustomerDto {
  @IsNotEmpty()
  @Length(1, 64)
  name: string;
  @IsNotEmpty()
  @Length(1, 64)
  surname: string;
}
